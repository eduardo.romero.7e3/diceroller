package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Array;

public class MainActivity extends AppCompatActivity {

    ImageView result1;
    ImageView result2;
    Button rollButton;
    Button unrollButton;
    boolean rolled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rollButton = findViewById(R.id.roll_button);
        unrollButton = findViewById(R.id.unroll_button);
        result1 = findViewById(R.id.result_imgView1);
        result2 = findViewById(R.id.result_imgView2);
        final int[] arr = {R.drawable.empty_dice, R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3,
                R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6};

        rollButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                // Toast.makeText(getApplicationContext(), R.string.working_toast, Toast.LENGTH_SHORT).show();
                rollButton.setText(R.string.rolled_button);
                int i = (int) (Math.round(Math.random() * 5) + 1);
                result1.setImageResource(arr[i]);
                int j = (int) (Math.round(Math.random() * 5) + 1);
                result2.setImageResource(arr[j]);
                rolled = true;
                if (i == 5 && j ==6) Toast.makeText(getApplicationContext(), "JACKPOT!", Toast.LENGTH_SHORT).show();
            }
        });

        unrollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!rolled){
                    Toast.makeText(getApplicationContext(), "Unroll what my dude", Toast.LENGTH_SHORT).show();
                }
                else{
                    result1.setImageResource(arr[0]);
                    result2.setImageResource(arr[0]);
                    rollButton.setText(R.string.roll_button);
                    rolled = false;
                }
            }
        });

        result1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (int) (Math.round(Math.random() * 5) + 1);
                result1.setImageResource(arr[i]);
            }
        });

        result2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (int) (Math.round(Math.random() * 5) + 1);
                result2.setImageResource(arr[i]);
            }
        });
    }
}